API Glossary
============

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: nutanix

.. autoclass:: ApiClient
    :members:

.. autoclass:: Prism
    :members:

.. autoclass:: Cluster
    :members:

.. autoclass:: Hosts
    :members:

.. autoclass:: Vms
    :members:

.. autoclass:: NutanixError

.. autoclass:: NutanixRestHTTPError

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
