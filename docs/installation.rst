Installation Guide
==================

The Nutanix Prism REST Client is available through the
Python Package Index.

The code is available on github and can optionally be built and
installed from source.


Python Package Index Installation
---------------------------------

.. code-block:: bash

    $ pip install nutanix

Or

.. code-block:: bash

    $ easy_install nutanix

