Quick Start Guide
=================

This guide is intended to give users a basic idea of REST Client usage
through examples.


Before You Begin
----------------

You should already have the Nutanix Prism REST Client installed.

This includes installing REST Client package dependencies.

See :doc:`installation` for more information.


Starting A Session
------------------

To verify the REST Client package is installed and importable, try executing
the following in a Python interpreter:

.. code-block:: python

    >>> import nutanix


If that succeeds without an ImportError, you are ready to start a REST session
using the client.

