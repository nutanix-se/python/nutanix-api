#!/usr/bin/python
# Copyright: (c) 2020, Ross Davies <ross.davies@nutanix.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

DOCUMENTATION = r'''
    name: nutanix_api.prism
    author:
        - Ross Davies <ross.davies@nutanix.com>

    short_description: Get & update data from Prism Element & Prism Central

    description:
        - Retrieve data from the API for the following API components
            - Prism UI
            - Prism Central Categories
            - Prism Central Tags
            - Clusters
            - Hosts
            - Virtual Machines

    requirements:
        - "python >= 3.5"
'''

EXAMPLES = r'''
'''


class Config(object):
    """
    docstring
    """

    def __init__(self, api_client):
        """
        docstring
        """
        self.api_client = api_client
        self.categories = []
        self.category_keys = []
        self.projects = []
        self.ui_config = []

    def get_ui_config(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/application/system_data'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.ui_config = self.api_client.request(uri=uri, api_version='v1', payload=payload, params=params)
        return self.ui_config

    def get_categories(self):
        """
        docstring
        """
        params = {}

        if self.api_client.connection_type == "pc":
            uri = '/categories/list'
            payload = '{ "kind":"category", "offset": 0, "length": 2147483647  }'
            self.categories = self.api_client.request(uri=uri, payload=payload, params=params).get(
                'entities')

        else:
            # pe does not have category data
            self.categories = {}

        return self.categories

    def get_category_keys(self, category):
        """
        docstring
        """
        params = {}

        if self.api_client.connection_type == "pc":
            uri = '/categories/{0}/list'.format(category)
            payload = '{ "kind":"category", "offset": 0, "length": 2147483647  }'
            self.category_keys = self.api_client.request(uri=uri, payload=payload, params=params).get(
                'entities')

        else:
            # pe does not expose category data
            self.category_keys = {}

        return self.category_keys

    def get_category_key_usage(self, category, key):
        """
        docstring
        """
        params = {}
        result = []

        if self.api_client.connection_type == "pc":
            uri = '/category/query'
            payload = {
                "group_member_count": 2147483647,
                "group_member_offset": 0,
                "usage_type": "APPLIED_TO",
                "category_filter": {
                    "type": "CATEGORIES_MATCH_ANY",
                    "kind_list": ["vm", "host"],
                    "params": {
                        category: key
                    }
                }
            }
            matches = self.api_client.request(uri=uri, payload=payload, params=params).get(
                'results')

            for match in matches:
                for kind_reference in match.get('kind_reference_list'):
                    item = {
                        "name": kind_reference.get('name'),
                        "uuid": kind_reference.get('uuid'),
                        "type": match.get('kind')
                    }
                    result.append(item)

        else:
            # pe does not expose category data
            pass

        return result

    def get_projects(self):
        """
        docstring
        """
        params = {}

        if self.api_client == "pc":
            uri = '/projects/list'
            payload = '{ "kind":"project", "offset": 0, "length": 2147483647  }'
            self.projects = self.api_client.request(uri=uri, payload=payload, params=params).get(
                'entities')

        else:
            # pe does not expose project data
            self.projects = {}

        return self.projects

    def get_project_usage(self, project_name):
        """
        docstring
        """
        params = {}
        result = []

        if self.api_client == "pc":
            uri = '/vms/list'
            payload = '{"kind": "vm", "offset": 0, "length": 2147483647 }'
            vms = self.api_client.request(uri=uri, payload=payload, params=params)

            for vm in vms:
                if vm.get('metadata'):
                    project_kind = vm.get('metadata').get('project_reference').get('kind')
                    vm_project_name = vm.get('metadata').get('project_reference').get('name')
                    if 'project_reference' in vm.get('metadata') and project_kind == 'project' and \
                            vm_project_name == project_name:
                        item = {
                            'name': vm.get('status').get('name'),
                            'uuid': vm.get('metadata').get('uuid')
                        }
                        result.append(item)

        else:
            # pe does not expose category data
            pass

        return result


class Cluster(object):
    """
    docstring
    """

    def __init__(self, api_client):
        self.api_client = api_client
        self.clusters = []
        self.auth_config = {}
        self.cluster_ha = {}

    def get_clusters(self):
        """
        docstring
        """
        params = {}
        payload = None

        if self.api_client.connection_type == "pc":
            uri = '/clusters/list'
            payload = '{"kind": "cluster", "offset": 0, "length": 2147483647 }'
        else:
            uri = '/clusters'

        clusters = self.api_client.request(uri=uri, payload=payload, params=params).get('entities')

        # Only return PE clusters ie. exclude any clusters defined as MULTICLUSTER or where the cluster name is not set
        cluster_list = []
        if self.api_client.connection_type == "pc":
            for cluster in clusters:
                if "PRISM_CENTRAL" not in cluster.get('status').get('resources').get('config').get(
                        'service_list') and cluster.get('status').get('name') != 'Unnamed':
                    cluster_list.append(cluster)

        return cluster_list

    def get_cluster(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/cluster'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        return self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params)

    def get_cluster_ha(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/ha'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.cluster_ha = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params)
        return self.cluster_ha

    def get_cluster_auth_config(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/authconfig'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.auth_config = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get('entities')
        return self.auth_config


class Hosts(object):
    """
    docstring
    """

    def __init__(self, api_client):
        self.api_client = api_client
        self.hosts = []

    def get_hosts(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/hosts'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.hosts = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get(
            'entities')
        return self.hosts


class Vms(object):
    """
    docstring
    """

    def __init__(self, api_client):
        self.api_client = api_client
        self.vms = []

    def get_vms(self, clusteruuid=None):
        """
        docstring
        """
        params = {'length': '2147483647'}
        payload = None
        uri = '/vms'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.vms = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get('entities')
        return self.vms


class Images(object):
    """
    docstring
    """

    def __init__(self, api_client):
        self.api_client = api_client
        self.images = []

    def get_images(self, clusteruuid=None):
        """
        docstring
        """
        params = {'length': '2147483647'}
        payload = None
        uri = '/images'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.images = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get('entities')
        return self.images


class Storage(object):
    """
    docstring
    """

    def __init__(self, api_client):
        self.api_client = api_client
        self.storage_containers = []
        self.volume_groups = []

    def get_storage_containers(self, clusteruuid=None):
        """
        docstring
        """
        params = {'count': '2147483647'}
        payload = None
        uri = '/storage_containers'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.storage_containers = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get('entities')
        return self.storage_containers

    def get_volume_groups(self, clusteruuid=None):
        """
        docstring
        """
        params = {}
        payload = None
        uri = '/volume_groups'

        if clusteruuid:
            params['proxyClusterUuid'] = clusteruuid

        self.volume_groups = self.api_client.request(uri=uri, api_version='v2.0', payload=payload, params=params).get('entities')
        return self.volume_groups
