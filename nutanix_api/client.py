#!/usr/bin/python
# Copyright: (c) 2020, Ross Davies <ross.davies@nutanix.com>
#
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

# import urllib3
from base64 import b64encode
import requests

DOCUMENTATION = r'''
    name: nutanix_api.client
    author:
        - Ross Davies <ross.davies@nutanix.com>

    short_description: Connect to the Nutanix Prism v2 or v3 API and return data

    description:
        - Connect to the API and test successful login

    requirements:
        - "python >= 3.5"
        - "requests >= 2.24.0"
'''

EXAMPLES = r'''
'''


class NutanixError(Exception):
    """Exception type raised by Nutanix API.
    :param reason: A message describing why the error occurred.
    :type reason: str
    :ivar str reason: A message describing why the error occurred.
    """

    def __init__(self, reason):
        self.reason = reason
        super(NutanixError, self).__init__(reason)

    def __str__(self):
        return "Error: {0}".format(self.reason)


class NutanixRestHTTPError(NutanixError):
    """Exception raised as a result of non-200 response status code.
    :param target: IP or DNS name of the array that received the HTTP request.
    :type target: str
    :param rest_version: The REST API version that was used when making the
                         request.
    :type rest_version: str
    :param response: The response of the HTTP request that caused the error.
    :type response: :class:`requests.Response`
    :ivar str target: IP or DNS name of the array that received the HTTP request.
    :ivar str rest_version: The REST API version that was used when making the
                            request.
    :ivar int code: The HTTP response status code of the request.
    :ivar dict headers: A dictionary containing the header information. Keys are
                        case-insensitive.
    :ivar str reason: The textual reason for the HTTP status code
                      (e.g. "BAD REQUEST").
    :ivar str text: The body of the response which may contain a message
                    explaining the error.
    .. note::
        The error message in text is not guaranteed to be consistent across REST
        versions, and thus should not be programmed against.
    """

    def __init__(self, target, rest_version, response):
        super(NutanixRestHTTPError, self).__init__(response.reason)
        self.target = target
        self.rest_version = rest_version
        self.code = response.status_code
        self.headers = response.headers
        self.text = response.text

    def __str__(self):
        msg = ("RestHTTPError status code {0} returned by REST "
               "version {1} at {2}: {3}\n{4}")
        return msg.format(self.code, self.rest_version, self.target,
                          self.reason, self.text)


class ApiClient(object):
    """
    docstring
    """

    def __init__(self, connection_type, ip_address, username="admin", password="nutanix/4u", port="9440",
                 validate_certs=False):
        self.encoded_credentials = b64encode(bytes('{0}:{1}'.format(username, password), encoding='ascii')).decode(
            'ascii')
        self.auth_header = 'Basic {0}'.format(self.encoded_credentials)

        self.validate_certs = validate_certs
        # if not validate_certs:
        #     urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        if connection_type not in ['pc', 'pe']:
            raise NutanixError('connection_type needs to be set to either pe or pc not {}.'.format(connection_type))
        self.connection_type = connection_type

        self.api_base = {'v1': 'https://{0}:{1}/PrismGateway/services/rest/v1'.format(ip_address, port),
                         'v2.0': 'https://{0}:{1}/PrismGateway/services/rest/v2.0'.format(ip_address, port)}
        if self.connection_type == "pc":
            self.api_base['v3'] = 'https://{0}:{1}/api/nutanix/v3'.format(ip_address, port)
            self._api_version = "v3"
        else:
            self._api_version = "v2.0"

    def request(self, uri, api_version=None, payload=None, params=None, response_code=200, timeout=120, method=None):
        """
        docstring
        """
        headers = {'Accept': 'application/json', 'Content-Type': 'application/json',
                   'Authorization': 'Basic {0}'.format(self.encoded_credentials), 'cache-control': 'no-cache'}

        try:
            if not api_version:
                api_version = self._api_version

            request_url = '{0}{1}'.format(self.api_base[api_version], uri)

            method = method or ('POST' if payload else 'GET')
            response = requests.request(method, request_url, headers=headers, verify=self.validate_certs,
                                        timeout=timeout,
                                        params=params)

        except requests.exceptions.RequestException as err:
            raise NutanixError(err)

        if response.status_code == response_code:
            return response.json()
        else:
            raise NutanixRestHTTPError(request_url, str(self._api_version), response)

    def test(self):
        """
        docstring
        """
        params = {}
        payload = None

        if self.connection_type == "pc":
            uri = '/clusters/list'
            payload = '{"kind": "cluster", "offset": 0, "length": 2147483647 }'
        else:
            uri = '/clusters'

        result = self.request(uri=uri, payload=payload, params=params)

        if result:
            return True
        else:
            return False
